# Vertretungsplan integriert in einem individuellen Stundenplan #

Entstanden im Rahmen von Jugend Hackt Deutschland 2014

Im grunde soll sie eine gute Alternative zu jetzigen Vertretungsplan Applikationen bringen,
durch:
- opt. Visualisierung von Änderungen direkt am Plan
- einfache Änderungen der Stunden

### Wie bring ich die Web-App zum laufen? ###

* source code downloaden
* mysql DB erstellen, mit Namen: (siehe Quellcode)
* localhost anschmeißen
* WICHTIG: Passwörter sind verschlüsselt
* Webseite aufrufen

### Kenntnisse ###

* PHP, mysql
* HTML, CSS
* jquery stylesheets (nicht zwingend)