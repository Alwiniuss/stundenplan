<html>
	<head>
		<title>Stundenplan</title>
		<link rel="stylesheet" type="text/css" href="plan.css"/>
		
		<link rel="stylesheet" href="jquery-ui/jquery-ui.min.css">
		<script src="jquery-ui/external/jquery/jquery.js"></script>
		<script src="jquery-ui/jquery-ui.min.js"></script>
	</head>
	<body>
		<?php
		include_once('db.php');
		//Wenn der Benutzer nicht eingeloggt ist zum Login weiterleiten
		if (!isset($_SESSION['id']))
		{
			header('Location: index.php');
			exit();  
		}
		?>
		<div id="mainmenu">
			<?php 
				//Wenn der Benutzer Lehrer ist den Link zur Stundenverwaltung anzeigen
				if ($_SESSION['type'] == "teacher")
				{
					echo "<div style='margin-right:10;'><a class='button' href='stundenplan-create.php'>Zur Stundenverwaltung</a></div>";
				}
			?>
			<!--<div><a class="button" href="print.php">Vertretungsplan ausdrucken</a></div>-->
			<div style="float: right;"><a class="button" href="logout.php">Logout</a></div>
		</div>
		<div id="kasten1">
			<h2>
			Stundenplan von 
			<?php		
			echo $_SESSION['firstname'] . " " . $_SESSION['lastname']; //Der Name des angemeldeten Benutzers
			?>
			</h2> 			
			<h3>
			<?php
			//Die aktuelle Kalenderwoche ermitteln
			$calendarWeek = (int)date('W');			
			$year = (int)date('Y');
			//Die letzte Kalenderwoche im Jahr ermitteln
			$lastCalWeek = (int)date("W", strtotime("31.12.".$year)); 
			if ($lastCalWeek == 1)
				$lastCalWeek = (int)date("W", strtotime("28.12.".$year)); 
			$weekday = (int)date('N');
			//Wenn der Wochentag Samstag oder Sonntag ist: Die nächste Woche anzeigen
			if ($weekday > 5)
			{
				$calendarWeek++;
				if ($calendarWeek > $lastCalWeek)
				{
					$calendarWeek = 1;
					$year++;
				}
			}	
			
			//Datum des Wochenbeginns - Freitag ausgeben
			$start = strtotime("$year-W$calendarWeek-1");
			echo date("d.m", $start) . " - " . date("d.m", $start + 4 * 24 * 60 * 60);
			?>
			</h3>
			<!--Die Stundenplan-Tabelle-->
			<table>
				<tr class="topHeader">
					<th class="header" colspan="2"></th>
					<td class="header">Montag</td>
					<td class="header">Dienstag</td>
					<td class="header">Mittwoch</td>
					<td class="header">Donnerstag</td>
					<td class="header">Freitag</td>
				</tr>
				<?php							
				$id = $_SESSION['id'];
				$periods = $connection->query("SELECT * FROM periods ORDER BY period ASC");
				$rCount = $periods->num_rows;
				
				$table = array(
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false),
					array(false, false, false, false, false)
				);
				
				//Zeile für Zeile die Tabelle füllen
				for ($row_no = 0; $row_no < $rCount; $row_no++) 
				{
					$periods->data_seek($row_no);
					$p_row = $periods->fetch_assoc();
					$period = $p_row['period'];
					$p_start = $p_row['start'];
					$p_end = $p_row['end'];
									
					$p_start = date("H:i", strtotime($p_start));
					$p_end = date("H:i", strtotime($p_end));						
						
					echo "<tr><td class='header'>$period</td>";
					echo "<td class='timeHeader'>$p_start - $p_end</td>";	
					
					if ($_SESSION['type'] == 'teacher')
					{
						$classes = $connection->query("SELECT end_period, weekday, room, name, subject, times.id as time_id FROM attendance INNER JOIN classes ON classes.class_id = attendance.class_id AND user_id = $id INNER JOIN times ON classes.class_id = times.class_id AND start_period = $period UNION SELECT end_period, weekday, times.room, name, subject, times.id as time_id FROM substitutions INNER JOIN times ON time_id = times.id AND start_period = $period AND teacher_id = $id AND calendar_week = $calendarWeek INNER JOIN classes ON classes.class_id = times.class_id ORDER BY weekday ASC");
					}
					else
					{
						$classes = $connection->query("SELECT end_period, weekday, room, name, subject, times.id as time_id FROM attendance INNER JOIN classes ON classes.class_id = attendance.class_id AND user_id = $id INNER JOIN times ON classes.class_id = times.class_id AND start_period = $period ORDER BY weekday ASC");
					}
					
					for ($i = 0; $i < $classes->num_rows; $i++) 
					{
						$classes->data_seek($i);
						$c_row = $classes->fetch_assoc();
						for ($wd = 0; $wd < $c_row['weekday'] - 1; $wd++)
						{
							if ($table[$period - 1][$wd] == false)
							{
								$table[$period - 1][$wd] = true;
								echo "<td></td>";
							}
						}
						
						$spot_blocked = false;					
						for ($j = $period; $j <= $c_row['end_period']; $j++)
						{
							if($table[$j - 1][$c_row['weekday'] - 1] == true)
							{
								$spot_blocked = true;
								break;
							}
						}
						
						if (!$spot_blocked)
						{
							$num_periods = $c_row['end_period'] - $period + 1;					
							$timeId = $c_row['time_id'];
							$substitution = $connection->query("SELECT * FROM substitutions WHERE calendar_week = $calendarWeek AND time_id = $timeId");
							if ($_SESSION['type'] != 'teacher' && $substitution->num_rows > 0)
							{
								$s_row = $substitution->fetch_assoc();
								$type = $s_row['type'];
							
								if ($type == "dropped")
								{
									echo "<th class='period' style='background-color: rgba(255,0,0,0.5);' title='Entf&auml;llt' rowspan='" . $num_periods . "'>" . $c_row['name'] . " " . $c_row['room'] . "</th>";
								}
								else if ($type == "substitution")
								{
									$teacherId = $s_row['teacher_id'];
									$teacherQ = $connection->query("SELECT firstName, lastName FROM users WHERE id = $teacherId");
									$t_row = $teacherQ->fetch_assoc();
									$teacher = $t_row['firstName'] . " " . $t_row['lastName'];
									
									if ($s_row['room'] != null && $c_row['room'] != $s_row['room'])
									{
										echo "<th class='period' style='background-color: rgba(255,255,0,0.5);' title='Wird vertreten von $teacher' rowspan='" . $num_periods . "'>" . $c_row['name'] . " <b>" . $s_row['room'] . "</b></th>";							
									}
									else
									{
										echo "<th class='period' style='background-color: rgba(255,255,0,0.5);' title='Wird vertreten von $teacher' rowspan='" . $num_periods . "'>" . $c_row['name'] . " " . $c_row['room'] . "</th>";
									}															
								}
								else if ($type == "room_substitution")
								{
									echo "<th class='period' style='background-color: rgba(255,255,0,0.5);' title='Raumvertretung' rowspan='" . $num_periods . "'>" . $c_row['name'] . " <b>" . $s_row['room'] . "</b></th>";
								}
								else
								{
									echo "<th class='period' rowspan='" . $num_periods . "'>Vertretung</th>";
								}
							}
							else
							{
								echo "<th class='period' rowspan='" . $num_periods . "'>" . $c_row['name'] . " " . $c_row['room'] . "</th>";
							}
							
							for ($j = $period; $j <= $c_row['end_period']; $j++)
							{
								$table[$j - 1][$c_row['weekday'] - 1] = true;
							}
						}				
					}
					
					for ($wd = 0; $wd < 5; $wd++)
					{			
						if ($table[$period - 1][$wd] == false)
						{
							$table[$period - 1][$wd] = true;
							echo "<td></td>";
						}
					}
					
					echo "</tr>";
				}
				?>
			</table>
			<!-- JQuery UI Tooltips -->
			<script>$( ".period" ).tooltip();</script>
		</div>
	</body>
</html>