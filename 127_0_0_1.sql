-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 15. Sep 2014 um 17:02
-- Server Version: 5.5.39
-- PHP-Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- Datenbank: `stundenplan`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `attendance`
--

INSERT INTO `attendance` (`user_id`, `class_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(3, 1),
(3, 2),
(3, 7),
(3, 11),
(4, 4),
(4, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
`class_id` int(11) NOT NULL,
  `name` varchar(16) NOT NULL,
  `year` int(11) NOT NULL,
  `subject` varchar(32) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Daten für Tabelle `classes`
--

INSERT INTO `classes` (`class_id`, `name`, `year`, `subject`) VALUES
(1, 'M1', 12, 'Mathematik'),
(2, 'E2', 12, 'Englisch'),
(3, 'L5', 12, 'Latein'),
(4, 'G8', 12, 'Geschichte'),
(5, 'PW7', 12, 'Politik und Wirtschaft'),
(6, 'INF5', 12, 'Informatik'),
(7, 'PH15', 12, 'Physik'),
(8, 'D5', 12, 'Deutsch'),
(9, 'SPO26', 12, 'Sport'),
(10, 'REV5', 12, 'Religion'),
(11, '', 0, '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `periods`
--

CREATE TABLE IF NOT EXISTS `periods` (
  `period` int(11) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `periods`
--

INSERT INTO `periods` (`period`, `start`, `end`) VALUES
(1, '07:45:00', '08:30:00'),
(2, '08:30:00', '09:15:00'),
(3, '09:35:00', '10:20:00'),
(4, '11:25:00', '11:10:00'),
(5, '11:25:00', '12:10:00'),
(6, '12:15:00', '13:00:00'),
(7, '13:00:00', '13:45:00'),
(8, '13:50:00', '14:35:00'),
(9, '14:40:00', '15:25:00'),
(10, '15:30:00', '16:15:00'),
(11, '16:20:00', '17:05:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `substitutions`
--

CREATE TABLE IF NOT EXISTS `substitutions` (
`id` int(11) NOT NULL,
  `calendar_week` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `room` varchar(16) NOT NULL,
  `type` enum('dropped','substitution','room_substitution','other') NOT NULL DEFAULT 'other',
  `comment` varchar(128) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `substitutions`
--

INSERT INTO `substitutions` (`id`, `calendar_week`, `time_id`, `teacher_id`, `room`, `type`, `comment`) VALUES
(1, 38, 6, 3, '', 'substitution', ''),
(2, 38, 1, NULL, '', 'dropped', ''),
(3, 38, 15, NULL, '159', 'room_substitution', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `times`
--

CREATE TABLE IF NOT EXISTS `times` (
`id` int(11) NOT NULL,
  `start_period` int(11) NOT NULL,
  `end_period` int(11) NOT NULL,
  `weekday` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `room` varchar(16) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94 ;

--
-- Daten für Tabelle `times`
--

INSERT INTO `times` (`id`, `start_period`, `end_period`, `weekday`, `class_id`, `room`) VALUES
(1, 1, 2, 1, 1, '22'),
(2, 1, 2, 2, 2, '204'),
(3, 1, 2, 4, 7, '28'),
(4, 2, 2, 5, 1, '22'),
(5, 3, 4, 1, 2, '201'),
(6, 3, 4, 2, 1, '22'),
(7, 3, 4, 3, 4, '202'),
(8, 3, 4, 5, 8, '209'),
(9, 5, 5, 3, 5, '201'),
(10, 5, 5, 5, 7, '25'),
(11, 5, 6, 4, 8, '152'),
(12, 7, 7, 2, 3, '202'),
(13, 7, 7, 3, 2, '201'),
(14, 7, 7, 5, 4, '202'),
(15, 8, 9, 1, 3, '203'),
(16, 8, 9, 4, 9, 'TH2'),
(17, 8, 9, 5, 5, '201'),
(18, 8, 10, 3, 6, '116'),
(19, 10, 10, 4, 3, '334'),
(23, 11, 11, 4, 4, '22'),
(24, 9, 9, 3, 4, '22'),
(25, 10, 10, 3, 4, '22'),
(26, 10, 10, 1, 6, '23'),
(27, 10, 10, 4, 6, '45'),
(28, 10, 10, 4, 6, '45'),
(29, 1, 1, 1, 4, '22'),
(30, 1, 1, 1, 4, '100'),
(31, 11, 11, 1, 4, '100'),
(32, 10, 10, 1, 6, '23'),
(33, 8, 8, 1, 6, '22'),
(34, 8, 8, 5, 6, '100'),
(35, 8, 8, 1, 6, '22'),
(36, 8, 8, 1, 6, '22'),
(37, 9, 9, 1, 6, '22'),
(38, 7, 7, 1, 6, '22');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'student'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `lastname`, `password`, `email`, `type`) VALUES
(1, 'Robert', 'Robert', 'Lasch', '098f6bcd4621d373cade4e832627b4f6', '', 'admin'),
(2, 'Test', 'Testus', 'Test', '098f6bcd4621d373cade4e832627b4f6', '', 'student'),
(3, 'Lehrer', 'Herr', 'Müller', '098f6bcd4621d373cade4e832627b4f6', '', 'teacher'),
(4, 'Tamino', 'Tamino', 'Sch', '098f6bcd4621d373cade4e832627b4f6', '', 'teacher');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
 ADD PRIMARY KEY (`user_id`,`class_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
 ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
 ADD PRIMARY KEY (`period`);

--
-- Indexes for table `substitutions`
--
ALTER TABLE `substitutions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `substitutions`
--
ALTER TABLE `substitutions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;--
-- Datenbank: `test`
--

DELIMITER $$
--
-- Prozeduren
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end$$

DELIMITER ;
--
-- Datenbank: `webauth`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_pwd`
--

CREATE TABLE IF NOT EXISTS `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_pwd`
--
ALTER TABLE `user_pwd`
 ADD PRIMARY KEY (`name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
