<!DOCTYPE html>
<html lang="de">
	<head>
		<title>Stundenplan</title>
                <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
                <link rel="stylesheet" type="text/css" href="css/signin.css"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
	</head>
	<body>	
		<div id="body" class="container">
                   
		<?php
		include_once('db.php');
		
		if (!isset($_SESSION['name']))
		{
			if (isset($_POST['name']) && isset($_POST['password']))
			{
				$name = $_POST['name'];     //Hier auf ungültige Zeichen testen oder mysql_escape_string nutzen!
				$password = md5($_POST['password']);
				$res = $connection->query("SELECT * FROM users WHERE name='$name' AND password='$password'");
				if ($res->num_rows > 0)
				{
					$res->data_seek(0);
					$row = $res->fetch_assoc();
					
					$_SESSION['id'] = $row['id'];
				 	$_SESSION['name'] = $name; 
					$_SESSION['firstname'] = $row['firstname'];
					$_SESSION['lastname'] = $row['lastname'];
					$_SESSION['type'] = $row['type'];
					echo "<script>window.location.reload();</script>";
				}
				else
				{
					echo 
					'<form class="form-signin" role="form" method="post">
                                            <h1 class="form-signin-heading">Login fehlgeschlagen!</h1>
                                            <div class="form-signin-heading">Der Benutzername oder das Passwort waren ung&uuml;ltig.</div>
                                            <input type="text" class="form-control" placeholder="Benutzername" name="name" required autofocus>
                                            <input type="password" class="form-control" placeholder="Passwort" name="password" required>
                                            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                                        </form>
					';
				}
			}
			else
			{
				echo 
				'<form class="form-signin" role="form" method="post">
                                            <h1 class="form-signin-heading">Login</h1>
                                            <input type="text" class="form-control" placeholder="Benutzername" name="name" required autofocus>
                                            <input type="password" class="form-control" placeholder="Passwort" name="password" required>
                                            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                                        </form>';
			}
		}
		else
		{
			header('Location: plan.php');
			exit();  
		}	
		?>
		</div>
	</body>
</html>